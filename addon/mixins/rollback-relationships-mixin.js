import EmberObject from '@ember/object';
import Mixin from '@ember/object/mixin';

export default Mixin.create({
  init() {
      this._super(...arguments);
      let originalRelations = EmberObject.create({});

      if (!Array.isArray(this.get('propertiesToRollBack'))) {
        this.set('propertiesToRollBack', []);
      }

      let objectToRollback = this.get(this.get('objectToRollback'));
      this.get('propertiesToRollBack').forEach(function(rel){
        originalRelations.set(rel, objectToRollback.get(rel));
      });

      this.set('originalRelations', originalRelations);
  },
  originalRelations: null,
  objectToRollback: "",
  propertiesToRollBack: null,
  propertiesToRollBackDeepCopy: null,
  actions: {
    rollbackRelationships: function() {
      let objectToRollback = this.get(this.get('objectToRollback'));
      let originalRelations = this.get('originalRelations');

      Object.keys(originalRelations).forEach(function(k){
          objectToRollback.set(k, originalRelations[k]);
      });
    }
  }
});
